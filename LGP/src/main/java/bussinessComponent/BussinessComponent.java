package bussinessComponent;

import java.io.IOException;

import pageElements.Homepage;

public class BussinessComponent extends ReusableFunctions implements Homepage{
	public void OkataLogin(String UserId, String Pwd) throws IOException, InterruptedException {
		SendTextIntoTextBox(oktaUserID_TextBox, UserId);
		Thread.sleep(4000);
		SendTextIntoTextBox(oktaPasswd_TextBox, Pwd);
		waitUntill_visibilityOfElement(oktaLogin_Button, 200);
		Thread.sleep(4000);
		clickElement(oktaLogin_Button);
	}
}
