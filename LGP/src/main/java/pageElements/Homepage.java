package pageElements;

import org.openqa.selenium.By;

public interface Homepage {
	
	public static By createNewLetter_img = By.xpath("//img[@alt='Create A New Letter']");
	public static By jobNumber_RButton = By.xpath("//input[@id='job-number-id']");
	public static By jobNumber_TxtBox = By.xpath("//input[@id='sl-search-input-id']");
	public static By recipientDetails_Btn = By.xpath("//input[@id='sl-search-button-id']");
	public static By moveRight_Btn = By.xpath("//img[@title='Click to move selected letter content']");
	public static By addcontentValidation_Text = By.xpath("//h2[text()='Add content to the letter']");
	public static By textinRightTxtBox = By.xpath("//div[@contenteditable='true']");
	public static By purchaseOrderTab = By.xpath("//a[text()='Purchase Order Amendment Letter']");
	public static By purchaseOrderJobNumber_TxtBox = By.xpath("//input[@id='poal-job-number-input-id']");
	public static By lookUp_Btn = By.xpath("//input[@id='poal-search-button-id']");
	public static By attachThisJobToTheLetter_Btn = By.xpath("//button[text()='Attach job(s) to the letter']");
	
	
	public static By oktaUserID_TextBox = By.xpath("//input[@id='okta-signin-username']");
	public static By oktaPasswd_TextBox = By.xpath("//input[@id='okta-signin-password']");
	public static By oktaLogin_Button = By.xpath("//input[@id='okta-signin-submit']");
}
