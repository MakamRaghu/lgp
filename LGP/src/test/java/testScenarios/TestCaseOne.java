package testScenarios;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import bussinessComponent.BussinessComponent;
import bussinessComponent.ReadExcelData;
import bussinessComponent.WritingDataIntoExcel;
import jxl.read.biff.BiffException;
import pageElements.Homepage;

public class TestCaseOne extends BussinessComponent implements Homepage{
	public String data;
	public ArrayList<String> datalist = new ArrayList<String>();
	public int v;
	@BeforeTest
	public void initializeBrowser() throws IOException, InterruptedException, BiffException {
		CalBrowser();
		OkataLogin("rmakam@c-sgroup.com","Lathu6909@#");
	}
	//@Test
	public void scenarioOne() throws IOException, InterruptedException, BiffException {
		ReadExcelData read = new ReadExcelData("LGPTemp.xls", "IWP - Standard Letter");
		WritingDataIntoExcel write = new WritingDataIntoExcel();
		clickElement(createNewLetter_img);
		clickElement(jobNumber_RButton);
		SendTextIntoTextBox(jobNumber_TxtBox, "606137");
		clickElement(recipientDetails_Btn);
		waitUntill_visibilityOfElement(addcontentValidation_Text, 30);
		Thread.sleep(3000);
		for(int i=195;i<206;i++) {
			moveToElement(By.xpath("//a[text()='"+read.readData(0, i)+"']/ancestor::li[1]/i"));
			clickElementUsingJS(By.xpath("//a[text()='"+read.readData(0, i)+"']/ancestor::li[1]/i"));
			Thread.sleep(3000);
			clickElementUsingJS(By.xpath("//a[text()='"+read.readData(0, i)+"']/ancestor::li/ul//a[text()='"+read.readData(1, i)+"']/ancestor::li[1]/i"));
			Thread.sleep(3000);
			clickElementUsingJS(By.xpath("//a[contains(@title,'"+read.readData(2, i)+"')]"));
			Thread.sleep(3000);
			clickElementUsingJS(moveRight_Btn);
			data =read.readData(4, i);
			Thread.sleep(5000);
			if(driver.findElement(By.xpath("(//div[@class='modal-content'])[13]")).isDisplayed()) {
				data = read.readData(4, i).replace("<p>", "red");
				
				List<WebElement> li = driver.findElements(By.xpath("//td/input"));
				int x=1;
				String dataset = null;
				do {
				for(WebElement lis : li) {
					if(lis.isDisplayed()) {
						String val = lis.getAttribute("name");
						List<WebElement> fields = driver.findElements(By.xpath("//label[@name='"+val+"-lbl']"));
						v = fields.size();
						dataset = driver.findElement(By.xpath("(//label[@name='"+val+"-lbl'])["+x+"]")).getText();
						lis.sendKeys("red");
						datalist.add(dataset);
						x++;
					}
				}
				}
				while(x<=v);
				//System.out.println(datalist);
				String execData = read.readData(5, i);
				String listString = "";
				for (String s : datalist)
				{
				    listString += s + "\t";
				}
				if(listString.trim().replaceAll("\\s+", " ").equals(execData.trim().replaceAll("\\s+", " "))) {
					WritingDataIntoExcel.writeexcel("LGPTempex.xlsx", "IWP - Standard Letter", "True", 6, i);
				}
				else {
					WritingDataIntoExcel.writeexcel("LGPTempex.xlsx", "IWP - Standard Letter", "False", 6, i);
				}
				datalist.removeAll(datalist);
				clickElement(By.xpath("(//input[@value='OK'])[4]"));
			}
			String text = getTextByLocator(textinRightTxtBox);
			WritingDataIntoExcel.writeexcel("LGPTempex.xlsx", "IWP - Standard Letter", text, 4, i);
			if(text.trim().replaceAll("\\s+", " ").equals(data.trim().replaceAll("\\s+", " "))) {
				WritingDataIntoExcel.writeexcel("LGPTempex.xlsx", "IWP - Standard Letter", "True", 5, i);
			}
			else {
				WritingDataIntoExcel.writeexcel("LGPTempex.xlsx", "IWP - Standard Letter", "False", 5, i);
			}
			driver.navigate().refresh();
			waitUntill_visibilityOfElement(addcontentValidation_Text, 30);
		}
	}
	@Test
	public void scenarioTwo() throws BiffException, IOException, InterruptedException {
		ReadExcelData read = new ReadExcelData("LGPTemp.xls", "CAL - POAL, All SBU");
		WritingDataIntoExcel write = new WritingDataIntoExcel();
		clickElement(createNewLetter_img);
		clickElement(purchaseOrderTab);
		SendTextIntoTextBox(purchaseOrderJobNumber_TxtBox, "618130");
		clickElement(lookUp_Btn);
		waitUntill_visibilityOfElement(attachThisJobToTheLetter_Btn, 20);
		clickElement(attachThisJobToTheLetter_Btn);
		for(int i=1;i<26;i++) {
		clickElementUsingJS(By.xpath("//a[text()='"+read.readData(0, i)+"']/ancestor::li[1]/i"));
		clickElementUsingJS(By.xpath("//a[contains(text(),'"+read.readData(1, i)+"')]"));
		Thread.sleep(3000);
		clickElementUsingJS(moveRight_Btn);
		data =read.readData(3, i);
		Thread.sleep(5000);
		if(driver.findElement(By.xpath("(//div[@class='modal-content'])[13]")).isDisplayed()) {
			data = read.readData(3, i).replace("<p>", "red");
			
			List<WebElement> li = driver.findElements(By.xpath("//td/input"));
			for(WebElement lis : li) {
				if(lis.isDisplayed()) {
					lis.sendKeys("red");
				}
			}
			clickElement(By.xpath("(//input[@value='OK'])[4]"));
		}
		String text = getTextByLocator(textinRightTxtBox);
		WritingDataIntoExcel.writeexcel("LGPTempex.xlsx", "CAL - POAL, All SBU", text, 3, i);
		if(text.trim().replaceAll("\\s+", " ").equals(read.readData(2, i).trim().replaceAll("\\s+", " "))) {
			WritingDataIntoExcel.writeexcel("LGPTempex.xlsx", "CAL - POAL, All SBU", "True", 4, i);
		}
		else {
			WritingDataIntoExcel.writeexcel("LGPTempex.xlsx", "CAL - POAL, All SBU", "False", 4, i);
		}
		driver.navigate().refresh();
		}
	}
}
